/*
 * @file		Card.java
 * @brief		カード
 * @author		Yoshiki Miyake
 * @date		2020.06.02
 *
 * 参考サイト → https://qiita.com/hirossyi73/items/cf8648c31898216312e5
 *
 * (c)2020 Yoshiki Miyake.
 */

package blackjack1;

import java.util.Random;
import java.util.Scanner;

public class Card {

	//マーク名の配列
	private String[] mark = { "スペード", "ダイヤ", "ハート", "クラブ" };
	//数字の配列
	private int[] no = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };



	public String[] getMark() {
		return mark;
	}

	public int[] getNo() {
		return no;
	}

	//数字を絵札に変えるメソッド
	public String NoString(int no) {
		switch (no) {
		case 1:
			return "A";
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		default:
			String non = String.valueOf(no);
			return non;
		}
	}

	//11以上のカードの数字を10に統一するメソッド
	public int Point(int no) {
		if (no == 11 || no == 12 || no == 13) {
			return 10;
		}
		return no;
	}

	//（プレイヤー）11以上のカードの数字を10に統一する・Aが出た時の1点か11点か選べるメソッド
	public int Playerpoint(int no) {
		if (no == 11 || no == 12 || no == 13) {
			return 10;
		} else if (no == 1) {
			System.out.println("Aが出ました！！！ L を入力すると11点になります");
			Scanner scan = new Scanner(System.in);
			String str = scan.nextLine();
			if (str.equals("L")) {
				return 11;
			} else {
				return 1;
			}
		}
		return no;
	}

	//（未完成）（ディーラー）11以上のカードの数字を10に統一する・Aが出た時の1点か11点かランダムで選ぶメソッド
	public int Dealerpoint(int no) {
		int sum = 0;
		if (no == 11 || no == 12 || no == 13) {
			return 10;
		} else if (no == 1) {
			Random random = new Random();
			int non = random.nextInt(2) + 1;
			if(non == 1) {
				return 1;
			} else if(non == 2) {
				return 11;
			}
		}
		return no;
	}
}
