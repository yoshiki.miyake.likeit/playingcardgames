/*
* @file		Blackjack.java
* @brief		ブラックジャック
* @author		Yoshiki Miyake
* @date		2020.06.02
*
* 参考サイト → https://qiita.com/hirossyi73/items/cf8648c31898216312e5
*
* (c)2020 Yoshiki Miyake.
*/

package blackjack1;

import java.util.Random;
import java.util.Scanner;

public class Blackjack {

	public static void main(String[] args) {

		System.out.println("★☆★☆★ ブラックジャックゲームを開始します ★☆★☆★");

		//カードクラスをインスタンス化
		Card card = new Card();

		int[] no = card.getNo();
		String[] mark = card.getMark();

		//ランダムインスタンス
		Random random = new Random();

		//プレイヤーの合計点を0点で初期化
		int sum1 = 0;

		//ディーラーの合計点を0点で初期化
		int sum2 = 0;

		//プレイヤーのカードを2枚引く
		System.out.println("プレイヤーのカードは、、、");
		for (int i = 1; i < 3; i++) {
			int no1 = random.nextInt(13);
			int mark1 = random.nextInt(4);
			System.out.println(mark[mark1] + "の" + card.NoString(no[no1]));
			sum1 += card.Playerpoint(no[no1]);
		}
		System.out.println("です。");

		//改行
		System.out.println();

		//ディーラーのカードを2枚引く  ※2枚目のカードは見せない。
		System.out.println("です。ディーラーのカードは、、、");
		for (int i = 1; i < 3; i++) {
			int no2 = random.nextInt(13);
			int mark2 = random.nextInt(4);
			if (i == 1) {
				System.out.println(mark[mark2] + "の" + card.NoString(no[no2]));
			} else {
				System.out.println("２枚目のカードは秘密♡");
			}
			sum2 += card.Point(no[no2]);
		}
		System.out.println("です。");

		//改行
		System.out.println();

		//プレイヤーの2枚引いた時点での合計点
		System.out.println("プレイヤーの合計点は、" + sum1 + "点です");

		//ディーラーの2枚引いた時点での合計点（確認用）
		//System.out.println("ディーラーの合計点は、" + sum2 + "です");

		//カードを引く繰り返し
		while (true) {

			//ディーラーが21を超えたら繰り返し終了
			if (sum2 > 21 && sum1 <= 21 && sum1 < sum2) {
				System.out.println();
				System.out.println("ディーラーの点数は" + sum2 + "点でバーストしました。！");
				break;
			}

			//プレイヤーが21を超えたら繰り返し終了
			if (sum1 > 21 && sum2 <= 21 && sum1 > sum2) {
				System.out.println();
				System.out.println("プレイヤーの点数は" + sum1 + "点でバーストしました！！！");
				break;
			}

			if (sum1 > 21 && sum2 > 21) {
				break;
			}

			System.out.println("Y　は一枚引く、N　は引かずに終了");

			//スキャナーインスタンス
			Scanner scanner = new Scanner(System.in);
			String str = scanner.nextLine();

			//プレイヤーがカードを引く場合
			if (str.equals("Y")) {

				//プレイヤー
				int pno1 = random.nextInt(13);
				int pmark1 = random.nextInt(4);
				int pno2 = no[pno1];
				String mark1 = mark[pmark1];
				System.out.println("出たカードは、" + mark1 + "の" + card.NoString(pno2) + "です！");
				int no2 = card.Playerpoint(pno2);
				sum1 += no2;
				System.out.println("プレイヤーの合計点は、" + sum1 + "点です！！");

				//ディーラー(17を超えるまで)
				if (sum2 < 17) {
					int non2 = random.nextInt(13);
					sum2 += card.Point(no[non2]);
				}
				//確認用
				//System.out.println("ディーラーの合計点は、" + sum2 + "です");

				//プレイヤーがカードを引かない場合
			} else if (str.equals("N")) {

				//ディーラーのみ(17を超えるまで)
				if (sum2 < 17) {
					int dno1 = random.nextInt(13);
					sum2 += card.Point(no[dno1]);
				}
				if (sum2 > 21 && sum1 < 21) {
					System.out.println();
					System.out.println("ディーラーの点数は" + sum2 + "点でバーストしました。！");
					break;
				}
				System.out.println("プレイヤーの合計点は、" + sum1 + "点です！！");
				//確認用
				//System.out.println("ディーラーの合計点は、" + sum2 + "です");
				break;

				//異なる値が入力された場合
			} else {
				System.out.println("YかNを入力してください！");
				continue;
			}

		}

		//最終的な合計点
		System.out.println();
		System.out.println("！！！！最終結果！！！！");
		System.out.println("プレイヤーの得点は、" + sum1 + "点です。");
		System.out.println("ディーラーの得点は" + sum2 + "点です。");

		if (sum1 == sum2 || (sum1 > 21 && sum2 > 21)) {
			System.out.println("共にバーストしました！！両者引き分け！！！");
		} else if (sum1 > sum2) {
			if (sum1 > 21 && sum2 <= 21) {
				System.out.println("プレイヤー負け！ディーラー勝ち");
			} else {
				System.out.println("プレイヤー勝ち！ディーラー負け");
			}
		} else {
			if (sum2 > 21 && sum1 <= 21) {
				System.out.println("プレイヤー勝ち！ディーラー負け");
			} else {
				System.out.println("プレイヤー負け！ディーラー勝ち");
			}
		}

		System.out.println("また遊んでね！！！！");
	}
}
